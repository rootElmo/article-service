# Article Service

**Article Service** is a small service which consists of a **Java** back-end, and an **Angular** front-end. The service scrapes articles from a selected news site's front-page, saves them to a database, and displays saved articles in the browser.

### Links to the separate services

**[Java back-end (Article Displayer)](https://gitlab.com/rootElmo/article-displayer)**

**[Angular front-end (Article Frontend)](https://gitlab.com/rootElmo/article-frontend)**